Our mission is to improve the lives of people with hearing loss through better hearing. We provide comprehensive hearing care services including complete audiological (hearing) evaluations, hearing loss rehabilitation, and counseling for adults.

Address: 102 West 8th North Street, Summerville, SC 29483, USA

Phone: 843-571-0744
